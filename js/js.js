var bandera = false;//indica si el juego inicio
    var turno = 0;//determina el turno
    var tab = new Array();//arreglo de botones
    window.onload = function(){
        var iniciar = document.getElementById("iniciar");
        iniciar.addEventListener("click",comenzar);
    }
    function comenzar(){
        bandera = true;
        var player1 = document.getElementById("player1");
        var player2 = document.getElementById("player2");
        if(player1.value==""){
            alert("Falta el nombre del Jugador 1");
            player1.focus();
        }else{
            if(player2.value==""){
                alert("Falta el nombre del Jugador 2");
                player2.focus();
            }else{
                tab[0] = document.getElementById("b0");
                tab[1] = document.getElementById("b1");
                tab[2] = document.getElementById("b2");
                tab[3] = document.getElementById("b3");
                tab[4] = document.getElementById("b4");
                tab[5] = document.getElementById("b5");
                tab[6] = document.getElementById("b6");
                tab[7] = document.getElementById("b7");
                tab[8] = document.getElementById("b8");
                for(var i=0; i<9; i++){
                    tab[i].className = "botonInicial";
                    tab[i].value = "I";
                }
                turno = 1;
                document.getElementById("turnoplayer").innerHTML = "Adelante player " + player1.value;
            }
        }
    }
    function colocar(boton){
        if(bandera==true){
            if(turno==1 && boton.value=="I"){
                turno=2;
                document.getElementById("turnoplayer").innerHTML = "Adelante player " + player2.value;
                boton.value = "X";
                boton.className = "botonplayer1";
            }else{
                if(turno==2 && boton.value=="I"){
                    turno=1;
                    document.getElementById("turnoplayer").innerHTML = "Adelante player " + player1.value;
                    boton.value = "O";
                    boton.className = "botonplayer2";
                }
            }
        }
        revisar();
    }
    function revisar(){//condicionales para ganar
        if ((tab[0].value=="X" && tab[1].value=="X" && tab[2].value=="X")
        || (tab[3].value=="X" && tab[4].value=="X" && tab[5].value=="X")
        || (tab[6].value=="X" && tab[7].value=="X" && tab[8].value=="X")
        || (tab[0].value=="X" && tab[3].value=="X" && tab[6].value=="X")
        || (tab[1].value=="X" && tab[4].value=="X" && tab[7].value=="X") 
        || (tab[2].value=="X" && tab[5].value=="X" && tab[8].value=="X")
        || (tab[0].value=="X" && tab[4].value=="X" && tab[8].value=="X")
        || (tab[2].value=="X" && tab[4].value=="X" && tab[6].value=="X")       
        ){
            alert("Felicidades Ganaste player "+player1.value);
            bandera = false;
        }
        if ((tab[0].value=="O" && tab[1].value=="O" && tab[2].value=="O")
        || (tab[3].value=="O" && tab[4].value=="O" && tab[5].value=="O")
        || (tab[6].value=="O" && tab[7].value=="O" && tab[8].value=="O")
        || (tab[0].value=="O" && tab[3].value=="O" && tab[6].value=="O")
        || (tab[1].value=="O" && tab[4].value=="O" && tab[7].value=="O") 
        || (tab[2].value=="O" && tab[5].value=="O" && tab[8].value=="O")
        || (tab[0].value=="O" && tab[4].value=="O" && tab[8].value=="O")
        || (tab[2].value=="O" && tab[4].value=="O" && tab[6].value=="O")       
        ){
            alert("Felicidades Ganaste player "+player2.value);
            bandera = false;
        } 
    }